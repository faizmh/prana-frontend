//
//  User.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import Foundation

class User {
    
    var id = 0
    var phoneNumber = ""
    var bloodGroup = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var shouldShareLocation = true
    var verificationCode = 0
    
    init() {
        
    }
    
    init(data: NSDictionary) {
        self.id = data["id"] as? Int ?? 0
        self.phoneNumber = data["phone_num"] as? String ?? ""
        self.bloodGroup = data["blood_group"] as? String ?? ""
        self.latitude = data["latitude"] as? Double ?? 0.0
        self.longitude = data["longitude"] as? Double ?? 0.0
        self.shouldShareLocation = data["enable_ping"] as? Bool ?? true
        self.verificationCode = data["verification_code"] as? Int ?? 0
    }
    
    class func requestVerificationCode(phoneNumber: String, completionHandler: () -> ()) {
        
        let params = ["phone_num": phoneNumber, "device_token": Model.sharedModel.deviceToken]
        
        ParseEngine.sharedEngine.postData(keyPath: "user", params: params) { (data, error) -> () in
            if let user = data?["user"] as? NSDictionary {
                Model.sharedModel.currentUser = User(data: user)
            }
            else {
                println("some error in requesting code")
            }
            
            completionHandler()
        }
    }
    
    func update(latitude: Double = 8.557, longitude: Double = 76.882, bloodGroup: String, shouldShareMyLocation: Bool = true, completionHandler: () -> ()) {
        
        let params = ["latitude": "\(latitude)", "longitude": "\(longitude)", "blood_group": bloodGroup, "share_my_location": "\(shouldShareLocation.hashValue)"]
        
        ParseEngine.sharedEngine.postData(keyPath: "profile_addl/\(Model.sharedModel.deviceToken)", params: params) { (data, error) -> () in
            if let user = data?["user"] as? NSDictionary {
                Model.sharedModel.currentUser = User(data: user)
            }
            
            completionHandler()
        }
    }
    
}