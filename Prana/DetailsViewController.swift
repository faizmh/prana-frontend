//
//  DetailsViewController.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

class DetailsViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDelegate, LocationDelegate {

    enum PickerMode: String {
        
        case Location = "Location"
        case Bloodgroup = "Bloodgroup"
    }

    var pickerMode = PickerMode.Location
    var currentTextField: UITextField?

    @IBOutlet var picker: UIPickerView!
    @IBOutlet weak var textFieldBloodGroup: UITextField!
    @IBOutlet weak var textFieldCurrentLocation: UITextField!
    @IBOutlet weak var notifySwitch: UISwitch!
    
    @IBOutlet var toolBar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldBloodGroup.inputView = picker
        textFieldCurrentLocation.inputView = picker
        
        textFieldBloodGroup.inputAccessoryView = toolBar
        textFieldCurrentLocation.inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
        getCurrentLocation()
    }
    
    func getCurrentLocation() {
        
        let locations = Locations.sharedLocations
        locations.locationDelegate = self
        locations.getCurrentLocation()
    }
    
    //location delegate 
    func currentLocation(latitude: Double, longitude: Double) {
        
        textFieldCurrentLocation.text = "Current Location"
    }
    
    // picker view delegates and datasource
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerMode {
            
        case .Location:
            return Locations.sharedLocations.locations.count
        case .Bloodgroup:
            return BloodGroup.allGroups.count
        default:
            break
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        switch pickerMode {
            
        case .Location:
            return Locations.sharedLocations.locations[row]
        case .Bloodgroup:
            return BloodGroup.allGroups[row].rawValue
        default:
            break
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        currentTextField?.text = pickerMode == PickerMode.Location ? Locations.sharedLocations.locations[row] : BloodGroup.allGroups[row].rawValue
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        currentTextField = textField
        
        pickerMode = textField == textFieldCurrentLocation ? PickerMode.Location : PickerMode.Bloodgroup
        picker.reloadAllComponents()
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func selectActionPicker(sender: UIBarButtonItem) {
        
        currentTextField?.text = pickerMode == PickerMode.Location ? Locations.sharedLocations.locations[picker.selectedRowInComponent(0)] : BloodGroup.allGroups[picker.selectedRowInComponent(0)].rawValue
        
        currentTextField?.resignFirstResponder()
    }
    
    @IBAction func switchActionNotify(sender: UISwitch) {
        currentTextField?.resignFirstResponder()
    }
    
    @IBAction func buttonActionStartSavingLives(sender: UIButton) {
        currentTextField?.resignFirstResponder()
        
        Model.sharedModel.currentUser.update(bloodGroup: textFieldBloodGroup.text, shouldShareMyLocation: notifySwitch.on) {
            self.appDelegate.loadHome()
        }

    }
    
    @IBAction func buttonActionSelectLocation(sender: UIButton) {
        getCurrentLocation()
    }
}
