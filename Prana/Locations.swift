//
//  Locations.swift
//  Prana
//
//  Created by mini on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationDelegate {
    func currentLocation(latitude: Double, longitude: Double)
}

class Locations: NSObject, CLLocationManagerDelegate {
    
    var locationDelegate: LocationDelegate?

    class var sharedLocations: Locations {
        struct Singleton {
            static let instance = Locations()
        }
        return Singleton.instance
    }
    
    var _locations: [String]?
    var locations: [String] {
        if _locations != nil { return _locations! }
        
        // load locations
        let path = NSBundle.mainBundle().pathForResource("district_list", ofType: "txt")
        let content = NSString(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)
        _locations = (content?.componentsSeparatedByString("\n") as! [String])
        return _locations!
    }
    
    
    //location manager autoupdate
    var locationManager: CLLocationManager?
    var isUpdatingLocation: Bool = false
    
    func getCurrentLocation() {
        
        locationManager = CLLocationManager()
        locationManager!.delegate = self
        locationManager!.distanceFilter = kCLDistanceFilterNone
        locationManager!.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        requestAlwaysAuthorization()
        
        if isLocationServiceEnabled
        {
            if !isUpdatingLocation
            {
                locationManager!.startUpdatingLocation()
                isUpdatingLocation = true
            }
            else{
            }
        }
        else
        {
        }
    }
    
    func requestAlwaysAuthorization() {
        
        locationManager?.requestAlwaysAuthorization()
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        var locValue : CLLocationCoordinate2D = manager.location.coordinate
        let lat = locValue.latitude
        let long = locValue.longitude
        println(long);
        println(lat);
        locationDelegate?.currentLocation(lat, longitude: long)
        isUpdatingLocation = false
        locationManager!.stopUpdatingLocation();
    }
    
    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager!) {
        isUpdatingLocation = false
    }
    
    func locationManagerDidResumeLocationUpdates(manager: CLLocationManager!) {
        isUpdatingLocation = true
    }
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
    
    }
    
    var isLocationServiceEnabled: Bool {
        
        let locationEnabled = CLLocationManager.locationServicesEnabled()
        let authorized = CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied
        return locationEnabled && !authorized
    }
}
