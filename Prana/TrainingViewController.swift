//
//  TrainingViewController.swift
//  Prana
//
//  Created by mini on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class TrainingViewController: UITableViewController, YTPlayerViewDelegate, UIWebViewDelegate {

    @IBOutlet weak var webViewVideo: UIWebView!
    @IBOutlet weak var levelProgressView: ProgressView!
    @IBOutlet weak var videoPlayerView: UIWebView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        levelProgressView.endAngle = 0.26
        
        loadYoutubeVideo()
    }
   
    func loadYoutubeVideo () {
    

        videoPlayerView.allowsInlineMediaPlayback = true
        videoPlayerView.mediaPlaybackAllowsAirPlay = false
        
        let embedHTML =  "<html><body><iframe src=\"https://www.youtube.com/watch?v=OSEw6kv70Xw?playsinline=1\" width=\"200\" height=\"200\" frameborder=\"0\" allowfullscreen></iframe></body></html>"
        videoPlayerView.loadHTMLString(embedHTML, baseURL: nil)
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        
    }

}
