//
//  UIColorExtensions.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func themeDarkGrayColor() -> UIColor {
        return UIColor.color(28.0, green: 28.0, blue: 28.0)
    }
    
    class func themeLightGrayColor() -> UIColor {
        return UIColor.color(50.0, green: 50.0, blue: 50.0)
    }
    
    class func themeHighlightColor() -> UIColor {
        return UIColor(red: 40.0/255.0, green: 180.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    }
    
    class func color(red: Double, green: Double, blue: Double, alpha: Double = 1.0) -> UIColor {
        return UIColor(red: CGFloat(red / 255.0), green: CGFloat(green / 255.0), blue: CGFloat(blue / 255.0), alpha: CGFloat(alpha))
    }
}