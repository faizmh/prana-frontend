//
//  Model.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import Foundation

class Meta {
    
    var status = ""
    var code = 200
    var message = ""
    
    init(data: NSDictionary) {
        self.status = data["status"] as? String ?? ""
        self.code = data["code"] as? Int ?? 200
        self.message = data["message"] as? String ?? ""
    }
}

class Model {
    
    class var sharedModel: Model {
        struct Singleton {
            static let sharedModel = Model()
        }
        return Singleton.sharedModel
    }
    
    var deviceToken = "fea255bb50e14f8dfd658859c1a346ba293cfcb507c8e68eab6104fd82f1f068"
    var currentUser = User()
    
    func requestVerificationCode(phoneNumber: String, completionHandler: () -> ()) {
        User.requestVerificationCode(phoneNumber) {
            completionHandler()
        }
    }
    
    func reportAccident(latitude: Double = 8.557, longitude: Double = 76.882, comments: String) {
        AccidentLog.reportAccident(latitude, longitude: longitude, comments: comments)
    }
    
    func requestBlood(latitude: Double = 8.557, longitude: Double = 76.882, bloodGroup: String, comments: String) {
        BloodRequest.requestBlood(latitude, longitude: longitude, bloodGroup: bloodGroup, comments: comments)
    }
}