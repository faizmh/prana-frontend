//
//  SendReportTableViewController.swift
//  Prana
//
//  Created by mini on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

class SendReportTableViewController: UITableViewController {

    @IBOutlet weak var buttonLocation: UIButton!
    @IBOutlet weak var textViewComments: UITextView!
    @IBOutlet weak var textFieldLocation: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureButton()
    }
    
    func configureButton() {
        
        buttonLocation.layer.cornerRadius = 35.0
        buttonLocation.layer.borderWidth = 3.0
        buttonLocation.layer.borderColor = UIColor.themeHighlightColor().CGColor
    }
    
    @IBAction func buttonActionCurrentLocation(sender: UIButton) {
        textFieldLocation.text = "Current Location"
    }
    
    @IBAction func barButtonActionSend(sender: UIBarButtonItem) {
        Model.sharedModel.reportAccident(comments: textViewComments.text)
        navigationController?.popToRootViewControllerAnimated(true)
    }
}
