//
//  AlertViewController.swift
//  Prana
//
//  Created by mini on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

class AlertListCell: UITableViewCell {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // configure the cell
        configureCell()
    }
    
    func configureCell() {
        
    }

}

class AlertViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {

    var arrayAlerts = [AlertRequest]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    struct AlertIdentifiers {
        
        // cell identifier
        static let cellIdentifierAlert = "kAlertCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension

        AlertRequest.listAlerts { (alerts) in
            self.arrayAlerts = alerts
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        AlertRequest.listAlerts { (alerts) in
            self.arrayAlerts = alerts
        }
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAlerts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("kAlertCell", forIndexPath: indexPath) as! UITableViewCell
        
        let alert = arrayAlerts[indexPath.row]
        cell.textLabel?.text = alert.comments
        cell.detailTextLabel?.text = alert.date
    
        cell.imageView?.image = UIImage(named: alert.type == "event" ? "ReportAccident" : "RequestBlood")
        
        return cell
    }

}
